---
name: The Conference Venue
---

# Venue
DebConf 23 will be held at [Athulya
hall](https://www.openstreetmap.org/way/240226049#map=19/10.00930/76.36155),
Infopark and [Four points by Sheraton
Kochi](https://www.openstreetmap.org/node/9658370139) hotel which are [300
metres](https://www.openstreetmap.org/directions?engine=fossgis_osrm_bike&route=10.00936%2C76.36158%3B10.00774%2C76.36278#map=18/10.00871/76.36232)
apart.

## Infopark - Athulya Hall
Athulya Hall inside the Infopark campus will be the main hall for the DebConf.

<img class="img img-fluid" src="{% static "img/athulya-exterior.jpg" %}"
     alt="Athulya hall, Infopark">
[Image source](https://commons.wikimedia.org/wiki/File:Athulya,_Infopark.jpg)

## Four Points Hotel
The Four Points Hotel where we have the accommodation will also have other halls
and hacklabs.

- Cinnamon 1
- Cinnamon 2
- Sage
- Board Room
- Businness Lounge
- All Spice

## Address

Four points by Sheraton Kochi  
Infopark Kochi Phase 1 Campus  
Infopark P.O, Kakkanad  
Kochi, Kerala, 682042

# Getting to Kochi
Cochin (Nedumbassery) International Airport, Cochin (IATA: COK, ICAO: VOCI) is
an international airport serving the city of Kochi, in the state of Kerala,
India. Located at Nedumbassery, about 25 kilometres (16 mi) northeast of the
city, Cochin International Airport is the busiest and largest airport in the
state of Kerala. It is also the fourth busiest airport in India in terms of
international traffic and eighth busiest overall.

[Kochi airport airlines and
destinations](https://en.wikipedia.org/wiki/Cochin_International_Airport#Airlines_and_destinations)

# Getting to venue

## From Cochin International Airport (`COK` / `CIAL`)

### Public transport
The venue is 27km from the Kochi airport.
There are no direct public transport links, all public transport options will include changeovers.

##### Metro / feeder  bus / water metro
The nearest metro station to airport is "Aluva".
Take a [metro feeder bus](https://kochimetro.org/feeder-service-time-table/)
from the airport to Aluva.
This service usually runs every 30 minutes and costs 60 INR
(accepts card payment). Please check
[timings](https://kochimetro.org/feeder-service-time-table/pawandoot-timings/).

Then there are several options:

- Option 1: Use [Kochi Metro](https://kochimetro.org/) from [Aluva metro
 station](https://kochimetro.org/metro-stations/) and get off at [Palarivattom
 metro station](https://kochimetro.org/metro-stations/) (the closest station for
 Inforpark). From there you can use Uber/Ola cabs to reach the hotel venue.
- Option 2: Instead of getting off at Palarivattom, you can get off at
 [Vytilla metro station](https://kochimetro.org/metro-stations/) (8 stops after
 Palarivattom). From there you can walk to [Vytilla **water metro**
 station](https://watermetro.co.in/) less than 1 minute away. There is water
 metro boat service from Vytilla water metro station to Kakkanad station in
 [every 40
 mins](https://preview.redd.it/vytilla-kakkanad-water-metro-timings-v0-dqzm3ceu8qwa1.jpg?width=1080&crop=smart&auto=webp&v=enabled&s=b86b3724be2d4ec5c2f169c1e71d4038e10950be).
 Once reaching Kakkanad station you can take a [feeder
 autorickshaw](https://th-i.thgim.com/public/incoming/o74vxc/article66786534.ece/alternates/LANDSCAPE_1200/water_metro_vyttila_kakkanad_01.jpg)
 to venue, which only takes around 15 mins. Main advantage of option 2 is, it's
 all **cheap and environmentaly friendly**.
- Option 3: There is a direct feeder bus from Aluva metro station to Infopark but
 it only runs twice a day. Once in the morning and again in the
 evening.[Timings](https://kochimetro.org/feeder-service-time-table/). **Note:**
 During working days, Aluva to Infopark feeder bus tend to more crowded.

Other options that show public transport for navigation:

- Google Maps (maps.google.com)
- OSMAnd app for android (Kochi metro only)

### Pre-paid Taxi
Pre-paid taxi would be the easiest and best way to reach the venue.
Infopark/Four Points Hotel is about 28 km from the the airport. The pre-paid
taxi would cost you around 1000-1200 INR (12-15 USD).

If you plan to book a pre-paid taxi, **you should pre-pay and book it before
exiting the arrival hall**. Once you exit, you cannot enter back in.

Pre-paid taxi counter is just after the Customs check, on the right side. Give
them the destination **Four Points Hotel, Info Park**.

Some points to remember while booking the prepaid taxi:

- **Payment can be done in card/cash**
- **Taxi should be taken 5-6 minutes after the payment is done**. So book the
 taxi only when you are ready to leave. If you are sharing with someone, wait
 for the last person to come before booking.
- **Taxi will collect you at prepaid taxi pillar A9** on the left side of the
 exit. You can find your taxi number on the receipt.

### Uber / OLA
App-based taxi services like Uber and OLA are also considerable options. You can
also book directly from the websites of [Uber](https://www.uber.com) and
[OLA](https://www.olacabs.com) if you don't want to install their proprietary
apps.

### Make My Trip / Goibibo
[Make My Trip](https://www.makemytrip.com/car-rental/cochin-airport-taxi.html)
and [Goibibo](https://www.goibibo.com/cars/cabs-from-kochi-airport) have options
to schedule pick up or drop at the airport. Choose "Four Points by Sheraton
Kochi Infopark" as destination. They offer free cancellation till 6 hours of
departure.

## From railway stations to Infopark

#### Public Transport
[ERS](https://en.wikipedia.org/wiki/Ernakulam_Junction_railway_station)
(Ernakulam Junction railway station) and
[ERN](https://en.wikipedia.org/wiki/Ernakulam_Town_railway_station) (Ernakulam
Town railway station) are the major railway stations in the city. The closest
metro stations are Ernakulam South metro station and Town hall metro station
respectively.  Though ERS to south metro station is a walkable distance, ERN to
Town hall metro station is not.

[Aluva Railway station](https://en.wikipedia.org/wiki/Aluva_railway_station)
is the third major after the above mentioned. Nearest metro station is Aluva.
Distance between both are not walkable.

- Option 1: Use MRT from Ernakulam South metro station or Town hall station and
 get off at Palarivattom Metro station. Can get autorickshaws or cab services
 to reach venue.
- Option 2: Get off at Vytilla metro station and walk to Vytilla **water**
 metro station. Use water metro service to reach Kakkanad station and use
 [electric feeder
auto](https://th-i.thgim.com/public/incoming/o74vxc/article66786534.ece/alternates/LANDSCAPE_1200/water_metro_vyttila_kakkanad_01.jpg)
from there to reach venue.

#### Cab service
As usual all online and offlines taxi services are available from railway
stations. If you are opting for offline local taxis, opt for prepaid ones at
the counters of railway station exits.
